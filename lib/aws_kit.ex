defmodule Manganese.AWSKit do
  alias Manganese.CoreKit

  @doc """

  Given a URL to an AWS resource, return the region.

  """
  @spec infer_region(binary) :: { :ok, binary } | { :error, CoreKit.Structs.APIError.t }
  def infer_region(url) do
    parsed_uri = URI.parse url
    host_fragments = String.split parsed_uri.host, "."

    case host_fragments do
      [ _service, region, "amazonaws", "com" ] -> { :ok, region }
      _ -> { :error, %CoreKit.Structs.APIError{
        type: :region_not_known,
        message: "The AWS region could not be inferred from a given URL",
        status: :internal_server_error
      }}
    end
  end
end
