defmodule Manganese.AWSKit.MixProject do
  use Mix.Project

  def project do
    [
      app: :manganese_aws_kit,
      version: "0.1.9",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [ :logger ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # ExDoc
      { :ex_doc, "~> 0.19", only: :dev, runtime: false },

      # Ecto
      { :ecto, "~> 3.0" },

      # Manganese CoreKit
      { :manganese_core_kit, "~> 0.1" }
    ]
  end

  defp package do
    [
      name: "manganese_aws_kit",
      description: "Library for using AWS data structures in Elixir",
      licenses: [ "Apache 2.0" ],
      links: %{
        "GitLab" => "https://gitlab.com/manganese/kits/aws-kit/aws-kit-elixir"
      },
      maintainers: [
        "Samuel Goodell"
      ]
    ]
  end
end
