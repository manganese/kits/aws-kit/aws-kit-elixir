defmodule Manganese.AWSKitTest do
  use ExUnit.Case

  alias Manganese.AWSKit

  doctest Manganese.AWSKit

  # Deserialization
  describe "`Manganese.AWSKit.infer_region/1`" do
    test "when it is a valid AWS URL it returns the region" do
      region = "us-west-2"
      url = "https://sqs." <> region <> ".amazonaws.com/123456789/manganese-aws-kit-queue"
      result = AWSKit.infer_region url

      assert result == { :ok, region }
    end

    test "when it is not a valid AWS URL it returns an error" do
      url = "http://mangane.se"
      result = AWSKit.infer_region url
      result_list = Tuple.to_list result

      assert Enum.at(result_list, 0) == :error
    end
  end
end
