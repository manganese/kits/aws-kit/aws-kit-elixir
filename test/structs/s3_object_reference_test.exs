defmodule Manganese.AWSKitTest.Structs.S3ObjectReferenceTest do
  use ExUnit.Case

  alias Manganese.AWSKit.Structs

  doctest Manganese.AWSKit.Structs.S3ObjectReference

  # Deserialization
  describe "`Manganese.AWSKit.Structs.S3ObjectReference.from_map/1`" do
    @tag s3_object_reference: true
    test "it deserializes a storage object reference from a map" do
      map = %{
        "region" => "us-west-2",
        "bucket" => "manganese_test",
        "path" => "aws-kit/test-file.txt"
      }
      reference = Structs.S3ObjectReference.from_map map

      assert reference.region == map["region"]
      assert reference.bucket == map["bucket"]
      assert reference.path == map["path"]
    end
  end

  # Serialization
  describe "`Manganese.AWSKit.Structs.S3ObjectReference.to_map/1`" do
    @tag s3_object_reference: true
    test "it serializes a storage object reference to a map" do
      reference = %Structs.S3ObjectReference{
        region: "us-west-2",
        bucket: "manganese_test",
        path: "aws-kit/test-file.txt"
      }
      map = Structs.S3ObjectReference.to_map reference

      assert map["region"] == reference.region
      assert map["bucket"] == reference.bucket
      assert map["path"] == reference.path
    end
  end

  describe "`Manganese.AWSKit.Structs.S3ObjectReference.to_url/1`" do
    @tag s3_object_reference: true
    test "it serializes a storage object reference to a URL" do
      reference = %Structs.S3ObjectReference{
        region: "us-west-2",
        bucket: "manganese_test",
        path: "aws-kit/test-file.txt"
      }
      url_string = Structs.S3ObjectReference.to_url reference
      url = URI.parse url_string

      assert url.host == "s3." <> reference.region <> ".amazonaws.com"
      assert url.path == "/" <> reference.bucket <> "/" <> reference.path
    end
  end
end
